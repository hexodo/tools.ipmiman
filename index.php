<?php
/**
 * IPMI management tool for HEXODO
 * Created by Cas de Reuver
 * Released under GPLv2 license
**/

require 'flight/Flight.php';
require 'functions.php';

$config = json_decode(file_get_contents('config.json'));

$token = $config->token;
$ipmitool = $config->ipmi_path;
$user = $config->user;
$password = $config->password;
define('B_C', $ipmitool.' -I lanplus -H :ip: -U "'.$user.'" -P "'.$password.'" ');

if(g_h('X-Token') != $token) {
  exit();
}

Flight::route('/power/status', function(){
  $command = r_c(g_h('X-Server'), 'chassis power status');
  $ret = ['status' => false, 'error' => ['Not able to switch']];
  if(!isset($command['out'][0])) {
    Flight::json($ret);
  }
  switch($command['out'][0]) {
    case 'Chassis Power is on':
      $ret = ['status' => true, 'power' => true];
      break;
    case 'Chassis Power is off':
      $ret = ['status' => true, 'power' => false];
      break;
  }
  Flight::json($ret);
});

Flight::route('/power/@action', function($action) {
  $do = '';
  $expect = '';
  switch($action) {
    case 'off_hard':
      $do = 'off';
      $expect = 'Chassis Power Control: Down/Off';
      break;
    case 'off_soft':
      $do = 'soft';
      $expect = 'Chassis Power Control: Soft';
      break;
    case 'on':
      $do = 'on';
      $expect = 'Chassis Power Control: Up/On';
      break;
    case 'reset':
      $do = 'reset';
      $expect = 'Chassis Power Control: Reset';
      break;
    case 'cycle':
      $do = 'cycle';
      $expect = 'Chassis Power Control: Cycle';
      break;
  }
  if(!$do) {
    Flight::json(['status' => false, 'error' => ['Unknown action']]);
  }
  $command = r_c(g_h('X-Server'), 'chassis power '.$do);
  $ret = ['status' => false, 'error' => ['Action failed']];
  if(isset($command['out'][0]) && $command['out'][0] == $expect) {
    $ret = ['status' => true, 'msg' => $command['out'][0]];
  }
  Flight::json($ret);
});

Flight::route('/temperature', function() {
  $command = r_c(g_h('X-Server'), 'sdr type "Temperature"');
  if(!isset($command['out'][0])) {
    Flight::json(['status' => false, 'error' => 'Could not connect to server']);
  }
  $temps = [];
  foreach($command['out'] as $line) {
    $line = explode('|', $line);
    $name = trim($line[0]);
    if(!isset($line[4])) {
      continue;
    }
    $reading = trim($line[4]);
    if(strpos($reading, 'Disabled') !== false || strpos($reading, 'No Reading') !== false) {
      continue;
    }
    $temps[$name] = str_replace(' degrees', '', $reading);
  }
  Flight::json($temps);
});

Flight::start();
?>
